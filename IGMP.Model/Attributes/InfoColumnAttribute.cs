﻿using System;

namespace IGMP.Model
{
    public class InfoColumnAttribute : Attribute
    {
        public int Index { get; set; }
        public string Caption { get; set; }
        public Type DataType { get; set; } = typeof(string);
    }

}