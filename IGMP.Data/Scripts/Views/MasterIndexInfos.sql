﻿CREATE OR ALTER VIEW [dbo].[View_MasterIndexInfos]
AS
SELECT			S.Id, S.PortId, S.Year, S.IndexNumber, S.IndexSerialNumber, S.ItemDescription, S.Number AS IGMNumber, F.IGMDate, S.FlightInfoId, S.MasterIndexInfoId AS IndexInfoId, M.IndexType,
						P.Name AS PortName, P.Code AS PortCode, P.SourceWebCode AS PortSourceWebCode, 
						F.FlightNumber, F.AirLine, F.ArrivalDate, 
						M.ImporterName, M.ConsignorName, M.BLNumber, M.BLDate, 
						M.Shed,
						SD.Code AS ShedCode, 
						SD.Name AS ShedName, 
						SD.SourceWebId AS ShedSourceWebId, 
						SD.SourceWebDesc AS ShedSourceWebDesc
FROM            dbo.SerialIndexInfos AS S
					INNER JOIN dbo.Ports AS P ON S.PortId = P.Id
					INNER JOIN dbo.FlightInfos AS F ON S.FlightInfoId = F.Id 
					INNER JOIN dbo.MasterIndexInfos AS M ON S.MasterIndexInfoId = M.Id
					LEFT JOIN dbo.ShedRefInfos AS SD ON M.ShedRefInfoId = SD.Id
