﻿namespace IGMP.Scraping.Scrapers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;

    public class HomeRequest : BaseRequest, IHomeRequest
    {
        public HomeRequest() : base()
        {
            this.Url = "/igm/IGMview1.aspx";
        }

        public string Send(IDictionary<string, object> parameters)
        {
            var request = WebRequest.Create(this.GetUrl());
            var response = request.GetResponse();
            var html = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return html;
        }
    }
}
