﻿namespace IGMP.Scraping.Scrapers
{
    using IGMP.Scraping.Constants;
    using IGMP.Model;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Web;

    public class IGMRequest : BaseRequest, IIGMRequest
    {
        public IGMRequest()
        {
            this.Url = "/igm/IGMview1.aspx";
            this.Method = "POST";
        }

        private string ViewState { get; set; }
        private string EventValidation { get; set; }
        private Port Port { get; set; }
        private int Number { get; set; }
        private int Year { get; set; }

        public string Send(IDictionary<string, object> parameters)
        {
            this.Populate(parameters);

            var request = WebRequest.Create(this.GetUrl());
            var postData = $"__VIEWSTATE={HttpUtility.UrlEncode(this.ViewState)}&" +
                            $"__EVENTVALIDATION={HttpUtility.UrlEncode(this.EventValidation)}&" +
                            $"PORTCD={HttpUtility.UrlEncode(this.Port.SourceWebCode)}&" +
                            $"IGMNO={this.Number}&" +
                            $"IGMCY={this.Year}&" +
                            $"Button2=View";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = request.GetResponse();
            var html = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return html;
        }

        private void Populate(IDictionary<string, object> dict)
        {
            this.ViewState = (string)dict[ScrapingParams.ViewState];
            this.EventValidation = (string)dict[ScrapingParams.EventValidation];
            this.Port = (Port)dict[ScrapingParams.Port];
            this.Number = (int)dict[ScrapingParams.Number];
            this.Year = (int)dict[ScrapingParams.Year];
        }
    }
}
