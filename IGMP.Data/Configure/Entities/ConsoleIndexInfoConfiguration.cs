﻿// using IGMP.Model;
// using Microsoft.EntityFrameworkCore;
// using Microsoft.EntityFrameworkCore.Metadata.Builders;

// namespace IGMP.Data
// {
//     public class ConsoleIndexInfoConfiguration : IEntityTypeConfiguration<ConsoleIndexInfo>
//     {
//         public void Configure(EntityTypeBuilder<ConsoleIndexInfo> builder)
//         {
//             builder.Property(e => e.Id).ValueGeneratedNever();
//             builder.HasOne(i => i.Port).WithMany().HasForeignKey(i => i.PortId).OnDelete(DeleteBehavior.Restrict);
//             builder.HasOne(i => i.FlightInfo).WithMany().HasForeignKey(i => i.FlightInfoId).OnDelete(DeleteBehavior.Restrict);
//         }
//     }
// }