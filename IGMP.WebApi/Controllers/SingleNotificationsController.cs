﻿using AutoMapper;
using IGMP.Model;
using IGMP.Service;
using IGMP.Service.Model;
using IGMP.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGMP.WebApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class SingleNotificationsController : ControllerBase {
        public SingleNotificationsController(
            INotificationService service,
            IMapper mapper
        ) {
            this.Service = service;
            this.Mapper = mapper;
        }

        private IMapper Mapper { get; set; }
        public INotificationService Service { get; private set; }

        [HttpGet]
        public ActionResult<PagedResult<SingleNotificationModel>> Get([FromQuery] PagingSortingParam queryParams)
        {
            var pagedSorted = queryParams.ToPagedSorted<NotificationParam>();
            pagedSorted.Parameters = new NotificationParam() { Archived = false };
            var results = this.Service.GetAllSingle(pagedSorted);
            return Ok(results);
        }

        [HttpGet("Archived")]
        public ActionResult<PagedResult<SingleNotificationModel>> GetArchived([FromQuery] PagingSortingParam queryParams)
        {
            var pagedSorted = queryParams.ToPagedSorted<NotificationParam>();
            pagedSorted.Parameters = new NotificationParam() { Archived = false };
            var results = this.Service.GetAllSingle(pagedSorted);
            return Ok(results);
        }

        [HttpGet("{id}")]
        public ActionResult<SingleNotificationModel> Get(int id)
        {
            var result = this.Service.GetSingle(id);
            return Ok(result);
        }

        [HttpPost]
        public void Post([FromBody] SingleNotificationModel model)
        {
            this.Service.AddSingle(model);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] SingleNotificationModel model)
        {
            this.Service.UpdateSingle(id, model);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.Service.Delete(id);
        }
    }
}