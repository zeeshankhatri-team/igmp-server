﻿using System.Threading.Tasks;

namespace IGMP.WebApi.Services
{
    public interface IEmailService
    {
        Task SendAsync(Email email);
    }
}
