﻿using System;
using System.IO;

namespace IGMP.Scraping
{
    public class ExecutionContext : IExecutionContext
    {
        public string Session { get; private set; }

        public string GetSessionDirectory()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scraping", this.Session);
        }

        public void StartSession()
        {
            this.Session = DateTime.Now.ToString("yyyyMMdd-hhmm-ss");
        }

        public void EndSession()
        {
            this.Session = null;
        }
    }
}
