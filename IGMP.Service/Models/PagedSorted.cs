namespace IGMP.Service.Model
{
    public class PagedSorted<T>
    {
        public T Parameters { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
    }

    public class PagingSortingParam
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SortBy { get; set; }
        public string Direction { get; set; }
    }
    
}