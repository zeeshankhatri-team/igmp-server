﻿using System;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using IGMP.Core;
using IGMP.WebApi.Model;
using Microsoft.AspNetCore.Identity;
using IGMP.WebApi.Auth.Model;
using System.Threading.Tasks;
using System.Linq;
using IGMP.WebApi.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Net;

namespace IGMP.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private UserManager<AppUser> UserManager { get; set; }
        private SignInManager<AppUser> SignInManager { get; set; }
        private IMapper Mapper { get; set; }
        private JWTSettings JWTSettings { get; set; }
        private IServiceProvider Provider { get; set; }

        public AuthController(
            IServiceProvider provider,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IMapper mapper,
            IOptions<JWTSettings> jwtSettings)
        {
            Provider = provider;
            UserManager = userManager;
            SignInManager = signInManager;
            Mapper = mapper;
            JWTSettings = jwtSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody]LoginModel model)
        {
            if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Password))
            {
                return BadRequest(new { errors = new string[] { "Email and password is required" } });
            }

            var user = await this.UserManager.FindByNameAsync(model.Email);
            if(user != null)
            {
                var result = await this.SignInManager.CheckPasswordSignInAsync(user, model.Password, true);
                if (result.Succeeded)
                {
                    return OkWithToken(user);
                }

                if (result.IsLockedOut)
                {
                    return BadRequest(new { errors = new string[] { "Email has been disabled for next 15 minutes due to 5 wrong attempts." } });
                }
            }

            return BadRequest(new { errors = new string[] { "Invalid email or password" } });

        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            // map dto to entity
            var user = Mapper.Map<AppUser>(model);

            try
            {
                var result = await this.UserManager.CreateAsync(user, model.Password);
                if(result.Succeeded)
                {
                    var origin = this.Request.Headers["Origin"];

                    var token = await this.UserManager.GenerateEmailConfirmationTokenAsync(user);
                    var body = $"<a href='{origin}/#/verify?email={user.Email}&token={WebUtility.UrlEncode(token)}'>Verify<a>";
                    var email = new Email() { To = user.Email, Body = body, Subject = "Verify your email addess" };
                    var service = this.Provider.GetRequiredService<IEmailService>();
                    await service.SendAsync(email);
                    return Ok();
                }
                else
                {
                    return BadRequest(new { errors = result.Errors.Select(e => e.Description)});
                }
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { errors = new string[] { ex.Message } });
            }
        }

        [AllowAnonymous]
        [HttpPost("verify")]
        public async Task<IActionResult> Verify([FromBody]VerifyModel model)
        {
            try
            {
                var user = await this.UserManager.FindByNameAsync(model.Email);
                if(user == null)
                {
                    return BadRequest(new { errors = new string[] { "Invalid request." } });
                }

                var result = await this.UserManager.ConfirmEmailAsync(user, model.Token);

                if (result.Succeeded)
                {
                    return OkWithToken(user);
                }
                else
                {
                    return BadRequest(new { errors = result.Errors.Select(e => e.Description) });
                }
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { errors = new string[] { ex.Message } });
            }
        }

        [AllowAnonymous]
        [HttpPost("forgot")]
        public async Task<IActionResult> Forgot([FromBody]EmailModel model)
        {
            try
            {
                var user = await this.UserManager.FindByNameAsync(model.Email);
                if (user == null)
                {
                    return this.Ok();
                }

                var origin = this.Request.Headers["Origin"];

                var token = await this.UserManager.GeneratePasswordResetTokenAsync(user);
                var body = $"<a href='{origin}/#/reset?email={user.Email}&token={WebUtility.UrlEncode(token)}'>Reset<a>";
                var email = new Email() { To = user.Email, Body = body, Subject = "Reset your password" };
                var service = this.Provider.GetRequiredService<IEmailService>();
                await service.SendAsync(email);
                return Ok();
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { errors = new string[] { ex.Message } });
            }
        }

        [AllowAnonymous]
        [HttpPost("reset")]
        public async Task<IActionResult> Reset([FromBody]ResetModel model)
        {
            try
            {
                var user = await this.UserManager.FindByNameAsync(model.Email);
                if (user == null)
                {
                    return BadRequest(new { errors = new string[] { "Invalid email or token" } });
                }

                var result = await this.UserManager.ResetPasswordAsync(user, model.Token, model.NewPassword);

                if (result.Succeeded)
                {
                    return OkWithToken(user);
                }
                else
                {
                    return BadRequest(new { errors = result.Errors.Select(e => e.Description) });
                }
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { errors = new string[] { ex.Message } });
            }
        }

        [HttpPost("change")]
        public async Task<IActionResult> Change([FromBody]ChangePasswordModel model)
        {
            try
            {
                var context = this.Provider.GetRequiredService<IRequestContext>();
                var user = await this.UserManager.FindByNameAsync(context.UserName);

                if (user == null)
                {
                    return BadRequest(new { errors = new string[] { "Invalid request." } });
                }

                var result = await this.UserManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);

                if (result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(new { errors = result.Errors.Select(e => e.Description) });
                }
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(new { errors = new string[] { ex.Message } });
            }
        }

        private IActionResult OkWithToken(AppUser user)
        {
            var token = BuildToken(user);

            // return basic user info (without password) and token to store client side
            return Ok(new { user.Email, user.FirstName, user.LastName, token });
        }

        private string BuildToken(AppUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(JWTSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserName.ToString()),
                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, user.Email.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                }),
                Expires = DateTime.UtcNow.AddDays(JWTSettings.Expires),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            return token;
        }

        //private string BuildToken1(AppUser user)
        //{
        //    var claims = new[] {
        //        new Claim(JwtRegisteredClaimNames.Sub, user.FullName),
        //        new Claim(JwtRegisteredClaimNames.Email, user.Email),
        //        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        //    };

        //    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppSettings.Secret));
        //    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        //    var token = new JwtSecurityToken(AppSettings.Issuer,
        //      AppSettings.Audience,
        //      claims,
        //      expires: DateTime.Now.AddMinutes(AppSettings.Expires),
        //      signingCredentials: creds);

        //    return new JwtSecurityTokenHandler().WriteToken(token);
        //}

    }
}
