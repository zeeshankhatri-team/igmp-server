
FROM microsoft/dotnet:latest AS base
WORKDIR /app

FROM microsoft/dotnet:latest AS build
WORKDIR /src
COPY IGMP.WebApi/IGMP.WebApi.csproj IGMP.WebApi/
RUN dotnet restore IGMP.WebApi/IGMP.WebApi.csproj
WORKDIR /src/IGMP.WebApi/
COPY . .
RUN dotnet build IGMP.WebApi/IGMP.WebApi.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish IGMP.WebApi/IGMP.WebApi.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "IGMP.WebApi.dll"]
