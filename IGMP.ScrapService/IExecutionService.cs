﻿using System.Threading.Tasks;

namespace IGMP.Scraping
{
    public interface IExecutionService
    {
        Task Run();
    }
}
