namespace IGMP.Model.Constants {
    public static class IndexType {
        public const char Master = 'M';
        public const char Console = 'C';
    }
}