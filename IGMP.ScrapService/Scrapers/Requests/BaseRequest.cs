﻿namespace IGMP.Scraping.Scrapers
{
    public abstract class BaseRequest
    {
        public string Host { get; set; } = "http://exportefiling.fbr.gov.pk";
        public string Url { get; set; }
        public string Method { get; set; }

        protected BaseRequest()
        {
            this.Url = "/";
            this.Method = "GET";
        }

        public string GetUrl()
        {
            return $"{this.Host}{this.Url}";
        }
    }
}