using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IGMP.Data
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> AsQueryable();

        IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includeProperties);

        int Count();

        int Count(Expression<Func<TEntity, bool>> predicate);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity Single(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity First(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);

        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void UpdateRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity, params string[] changedPropertyNames);

        void UpdateRange(IEnumerable<TEntity> entities, params string[] changedPropertyNames);
        
        bool Any(Expression<Func<TEntity, bool>> predicate);
    }
}