﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using IGMP.Core;
using IGMP.Data;
using IGMP.Model;
using IGMP.Service.Constants;
using IGMP.Service.Extensions;
using IGMP.Service.Model;

namespace IGMP.Service
{
    public class NotificationService : INotificationService {
        public NotificationService (IRequestContext requestContext, IUnitOfWork unitOfWork, IRepository<Notification> repository, IMapper mapper) {
            this.RequestContext = requestContext;
            this.UnitOfWork = unitOfWork;
            this.Repository = repository;
            this.Mapper = mapper;
        }

        public IMapper Mapper { get; private set; }

        public IRequestContext RequestContext { get; }
        public IUnitOfWork UnitOfWork { get; }
        public IRepository<Notification> Repository { get; }

        public PagedResult<ContinuousNotificationModel> GetAllContinuous(PagedSorted<NotificationParam> pagedSorted)
        {
            return GetAll<ContinuousNotificationModel>(pagedSorted, NotificationType.Continuous);
        }

        public PagedResult<SingleNotificationModel> GetAllSingle(PagedSorted<NotificationParam> pagedSorted)
        {
            return GetAll<SingleNotificationModel>(pagedSorted, NotificationType.Single);
        }

        public void AddContinuous(ContinuousNotificationModel model)
        {
            var notification = new Notification()
            {
                UserId = this.RequestContext.UserId,
                NotificationType = NotificationType.Continuous,
                ImporterName = model.ImporterName,
                ConsignorName = model.ConsignorName,
                ItemDescription = model.ItemDescription
            };

            this.Repository.Add(notification);
        }

        public void AddSingle(SingleNotificationModel model)
        {
            var notification = new Notification()
            {
                UserId = this.RequestContext.UserId,
                NotificationType = NotificationType.Single,
                BLNumber = model.BLNumber,
            };

            this.Repository.Add(notification);
            this.UnitOfWork.Commit();
        }

        public void UpdateContinuous(int id, ContinuousNotificationModel model)
        {
            var notification = this.GetNotification(id, NotificationType.Continuous);

            notification.ImporterName = model.ImporterName;
            notification.ConsignorName = model.ConsignorName;
            notification.ItemDescription = model.ItemDescription;

            this.Repository.Update(notification);
            this.UnitOfWork.Commit();
        }

        public void UpdateSingle(int id, SingleNotificationModel model)
        {
            var notification = this.GetNotification(id, NotificationType.Continuous);

            notification.BLNumber = model.BLNumber;

            this.Repository.Update(notification);
            this.UnitOfWork.Commit();
        }

        public void Delete(int id)
        {
            var notification = this.GetNotification(id);
            this.Repository.Remove(notification);
            this.UnitOfWork.Commit();
        }

        public void Archive(int id)
        {
            var notification = this.GetNotification(id);
            notification.Archived = true;
            this.Repository.Update(notification, nameof(Notification.Archived));
            this.UnitOfWork.Commit();
        }

        public ContinuousNotificationModel GetContinuous(int id)
        {
            return Get<ContinuousNotificationModel>(id, NotificationType.Continuous);
        }

        public SingleNotificationModel GetSingle(int id)
        {
            return Get<SingleNotificationModel>(id, NotificationType.Single);
        }

        private T Get<T>(int id, NotificationType type)
        {
            var notification = this.Repository.SingleOrDefault(n => n.Id == id && n.NotificationType == type);
            if (notification == null)
            {
                throw new ServiceException($"No notification found against id: {id}");
            }

            var model = this.Mapper.Map<T>(notification);
            return model;
        }

        private Notification GetNotification(int id, NotificationType type)
        {
            var notification = this.Repository.SingleOrDefault(n => n.Id == id && n.NotificationType == type);
            if (notification == null)
            {
                throw new ServiceException($"No notification found against id: {id}");
            }

            return notification;
        }

        private Notification GetNotification(int id)
        {
            var notification = this.Repository.SingleOrDefault(n => n.Id == id);
            if (notification == null)
            {
                throw new ServiceException($"No notification found against id: {id}");
            }

            return notification;
        }

        private PagedResult<T> GetAll<T>(PagedSorted<NotificationParam> pagedSorted, NotificationType type)
        {
            var query = this.Repository.AsQueryable();
            query = query.Where(n => n.NotificationType == type
                && n.Archived == (pagedSorted.Parameters != null && pagedSorted.Parameters.Archived));

            if (!string.IsNullOrWhiteSpace(pagedSorted.SortBy))
            {
                query = query.OrderByField(pagedSorted.SortBy, pagedSorted.Direction != SortDirection.Descending);
            }

            var length = query.Count();
            var list = query.Skip(pagedSorted.PageIndex * pagedSorted.PageSize).Take(pagedSorted.PageSize).ToList();
            var result = this.Mapper.Map<IEnumerable<T>>(list);
            var pagedResult = new PagedResult<T>(result, length);
            return pagedResult;
        }
    }
}