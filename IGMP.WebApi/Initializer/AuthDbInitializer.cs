﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IGMP.WebApi.Initializer
{
    public static class AuthDbInitializer
    {

        public static void Initialize(AuthDbContext dbContext, ILogger logger)
        {
            if (dbContext.Database.EnsureCreated())
            {
                logger.LogInformation("Database created");
            }

            dbContext.Database.Migrate();
        }
    }
}
