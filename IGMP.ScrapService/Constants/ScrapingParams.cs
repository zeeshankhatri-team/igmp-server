﻿namespace IGMP.Scraping.Constants
{
    public static class ScrapingParams
    {
        public const string ViewState = "ViewState";
        public const string EventValidation = "EventValidation";
        public const string Ports = "Ports";
        public const string Sheds = "Sheds";

        public const string Port = "Port";
        public const string Year = "Year";
        public const string Number = "Number";

        public const string Count = "Count";
    }

}