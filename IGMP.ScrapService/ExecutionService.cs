﻿using System;
using System.Collections.Generic;
using System.Linq;
using IGMP.Scraping.Constants;
using IGMP.Scraping.Scrapers;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using IGMP.Model;
using IGMP.Data;

namespace IGMP.Scraping
{
    public class ExecutionService : IExecutionService
    {
        private IExecutionContext Context { get; set; }
        private IServiceProvider ServiceProvider { get; set; }
        private AppDbContext DbContext { get; set; }
        private ShedService ShedService { get; set; }


        public ExecutionService(
            IExecutionContext context,
            IServiceProvider serviceProvider,
            AppDbContext dbContext,
            ShedService shedService
            )
        {
            Context = context;
            ServiceProvider = serviceProvider;
            DbContext = dbContext;
            ShedService = shedService;
        }

        public Task Run()
        {
            Context.StartSession();

            var existingPorts = this.DbContext.Ports.ToList();

            var home = ServiceProvider.GetService<IHomeScraper>();

            var homeDict = new Dictionary<string, object>();
            homeDict.Add(ScrapingParams.Ports, existingPorts);

            var result = home.Scrap(homeDict);
            var tasks = new List<Task>();

            if (result != null)
            {
                var ports = (IList<Port>)result[ScrapingParams.Ports];
                // foreach (var port in ports.Where(p => p.Code == "KPQI"))
                foreach (var port in ports)
                {
                    tasks.Add(
                        Task.Run(() =>
                        {
                            Dictionary<string, object> igmDict = CreateParams(result, port);
                            var igm = ServiceProvider.GetService<IIGMScraper>();
                            igm.Scrap(igmDict);
                        })
                    );
                }
            }

            return Task.WhenAll(tasks);
        }

        private static Dictionary<string, object> CreateParams(IDictionary<string, object> result, Port port)
        {
            return new Dictionary<string, object>(result)
            {
                { ScrapingParams.Port, port },
                { ScrapingParams.Year, 2018 }
            };
        }
    }
}
