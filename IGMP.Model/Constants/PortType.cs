﻿namespace IGMP.Model.Constants
{
    public static class PortType
    {
        public const char Sea = 'S';
        public const char Air = 'A';
    }

}
