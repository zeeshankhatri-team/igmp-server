﻿using HtmlAgilityPack;
using IGMP.Model;
using IGMP.Model.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using IGMP.Scraping.Extensions;
using IGMP.Data;
using IGMP.Scraping.Constants;
using AutoMapper;
using IGMP.Data.Constants;

namespace IGMP.Scraping.Scrapers
{
    public class IGMHandler : IIGMHandler
    {
        private HtmlDocument Document { get; set; }
        private IExecutionContext Context { get; set; }
        private ICsvAdapter CsvAdapter { get; set; }
        private IMapper Mapper { get; set; }
        private ShedService ShedService { get; set; }
        private AppDbContext DbContext { get; set; }

        private Port Port { get; set; }
        private int Year { get; set; }
        private int Number { get; set; }

        public IGMHandler(
            IExecutionContext context, 
            ICsvAdapter csvAdapter, 
            AppDbContext dbContext,
            IMapper mapper,
            ShedService shedService)
        {
            this.Context = context;
            this.Document = new HtmlDocument();
            this.CsvAdapter = csvAdapter;
            this.Mapper = mapper;

            this.DbContext = dbContext;
            this.ShedService = shedService;
        }
        
        public IDictionary<string, object> Handle(string response, IDictionary<string, object> parameters)
        {
            this.Document.LoadHtml(response);
            this.Populate(parameters);

            var flightList = this.ScrapHtmlTable<FlightInfo>();
            var count = flightList.Count;

            if(count > 0)
            {
                var shedList = this.ScrapHtmlTable<ShedRefInfo>();
                var consoleIndexList = this.ScrapHtmlTable<ConsoleIndexInfo>();
                var masterIndexList = this.ScrapHtmlTable<MasterIndexInfo>();
                var serialIndexList = this.ScrapHtmlTable<SerialIndexInfo>();

                var directory = Path.Combine(Context.GetSessionDirectory(), Year.ToString(), Port.Code.Trim(), Number.ToString());
                
                CsvAdapter.Append(directory, flightList);
                CsvAdapter.Append(directory, shedList);
                CsvAdapter.Append(directory, consoleIndexList);
                CsvAdapter.Append(directory, masterIndexList);
                CsvAdapter.Append(directory, serialIndexList);

                var flightInfo = flightList[0];
                flightInfo.Id = this.DbContext.GetNextValue(Sequences.FlightInfoId);
                SetFlightInfoId(consoleIndexList, masterIndexList, serialIndexList, flightInfo);

                var shedsInMaster = masterIndexList.Select(i => i.Shed).Distinct();

                ShedService.Extract(shedList, this.Port, this.Year, this.Number);
                ShedService.Extract(shedsInMaster, this.Port, this.Year, this.Number);

                SetMasterIndexInfoId(masterIndexList, serialIndexList, flightInfo, IndexType.Master);
                SetShedRefInfoId(masterIndexList);

                var consoleIndexListMapped = Mapper.Map<IList<MasterIndexInfo>>(consoleIndexList);
                SetMasterIndexInfoId(consoleIndexListMapped, serialIndexList, flightInfo, IndexType.Console);

                this.DbContext.FlightInfos.Add(flightInfo);
                this.DbContext.MasterIndexInfos.AddRange(masterIndexList);
                this.DbContext.MasterIndexInfos.AddRange(consoleIndexListMapped);
                this.DbContext.SerialIndexInfos.AddRange(serialIndexList);

                this.DbContext.SaveChangesAsync();
            }

            var dict = new Dictionary<string, object>();
            dict.Add(ScrapingParams.Count, count);

            return dict;
        }

        private void SetShedRefInfoId(IList<MasterIndexInfo> masterIndexList)
        {
            foreach (var masterIndexInfo in masterIndexList)
            {
                masterIndexInfo.ShedRefInfoId = ShedService.GetShedRefInfoId(masterIndexInfo.Shed);
            }
        }


        private void SetMasterIndexInfoId(IList<MasterIndexInfo> masterIndexList, IList<SerialIndexInfo> serialIndexList, FlightInfo flightInfo, char indexType)
        {
            foreach (var masterIndexInfo in masterIndexList)
            {
                var masterSerialIndexList = serialIndexList.Where(i => i.IndexNumber == masterIndexInfo.IndexNumber);
                foreach (var serialIndexInfo in masterSerialIndexList)
                {
                    serialIndexInfo.MasterIndexInfoId = masterIndexInfo.Id;
                }

                masterIndexInfo.IndexType = indexType;
            }
        }

        // TODO: Need to be refactor
        private void SetFlightInfoId(IList<ConsoleIndexInfo> consoleIndexList, IList<MasterIndexInfo> masterIndexList, IList<SerialIndexInfo> serialIndexList, FlightInfo flightInfo)
        {
            foreach (var MasterIndexInfo in masterIndexList)
            {
                MasterIndexInfo.Id = this.DbContext.GetNextValue(Sequences.MasterIndexInfoId);
                MasterIndexInfo.FlightInfoId = flightInfo.Id;
            }

            foreach (var consoleIndexInfo in consoleIndexList)
            {
                consoleIndexInfo.Id = this.DbContext.GetNextValue(Sequences.MasterIndexInfoId);
                consoleIndexInfo.FlightInfoId = flightInfo.Id;
            }

            foreach (var serialIndexInfo in serialIndexList)
            {
                serialIndexInfo.Id = this.DbContext.GetNextValue(Sequences.SerialIndexInfoId);
                serialIndexInfo.FlightInfoId = flightInfo.Id;
            }
        }

        private void Populate(IDictionary<string, object> parameters)
        {
            this.Port = (Port) parameters[ScrapingParams.Port];
            this.Year = (int)parameters[ScrapingParams.Year];
            this.Number = (int) parameters[ScrapingParams.Number];
        }

        private IList<T> ScrapHtmlTable<T>() where T : BaseInfo, new()
        {
            var list = new List<T>();
            var tableId = typeof(T).GetAttributeValue((InfoAttribute info) => info.HtmlTableId);
            var firstColumnIndex = typeof(T).GetAttributeValue((InfoAttribute info) => info.FirstColumnIndex);

            var htmlRows = this.Document.GetElementbyId(tableId).Elements("tr").Skip(1);
            var count = htmlRows.Count();

            if (count > 0)
            {
                foreach (var htmlRow in htmlRows)
                {
                    var row = this.ScrapHtmlRow<T>(htmlRow, firstColumnIndex);
                    list.Add(row);
                }
            }

            return list;
        }

        private T ScrapHtmlRow<T>(HtmlNode row, int firstColumnIndex) where T : BaseInfo, new()
        {
            var columns = row.Elements("td").Skip(firstColumnIndex);
            var values = columns.Select(c => c.InnerText).GetEnumerator();

            var data = new T();

            for(var index = 0; values.MoveNext() == true; index++)
            {
                var value = values.Current;
                var property = typeof(T).GetProperties().Single(p => p.GetAttributeValue((InfoColumnAttribute info) => info.Index) == index);
                var dataType = property.GetAttributeValue((InfoColumnAttribute info) => info.DataType);
                property.SetValue(data, this.ParseValue(dataType, value));
            }

            data.PortId = this.Port.Id;
            data.PortWebSourceCode = this.Port.SourceWebCode;
            data.Year = this.Year;
            data.Number = this.Number;

            return data;
        }

        private object ParseValue(Type dataType, string value)
        {
            object result;

            if (dataType == typeof(int))
            {
                result = int.Parse(value);
            }
            else if (dataType == typeof(float))
            {
                result = float.Parse(value);
            }
            else if (dataType == typeof(DateTime))
            {
                result = this.ParseDate(value);
            }
            else
            {
                result = value;
            }

            return result;
        }

        private DateTime? ParseDate(string value)
        {
            if (value.Length == 8)
            {
                var year = int.Parse(value.Substring(0, 4));
                var month = int.Parse(value.Substring(4, 2));
                var day = int.Parse(value.Substring(6, 2));

                return new DateTime(year, month, day);
            }

            return null;
        }
    }
}