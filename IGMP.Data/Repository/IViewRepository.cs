using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IGMP.Data
{
    public interface IViewRepository<TViewEntity>
        where TViewEntity : class
    {
        IQueryable<TViewEntity> AsQueryable();

        int Count(Expression<Func<TViewEntity, bool>> predicate);

        IEnumerable<TViewEntity> Find(Expression<Func<TViewEntity, bool>> predicate);

        TViewEntity Single(Expression<Func<TViewEntity, bool>> predicate);

        TViewEntity SingleOrDefault(Expression<Func<TViewEntity, bool>> predicate);

        TViewEntity First(Expression<Func<TViewEntity, bool>> predicate);

        TViewEntity FirstOrDefault(Expression<Func<TViewEntity, bool>> predicate);

    }
}