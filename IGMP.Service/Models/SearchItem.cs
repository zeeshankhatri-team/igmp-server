﻿using System;

namespace IGMP.Service.Model
{
    public class SearchItem
    {
        public int Id { get; set; }
        public string IndexNumber { get; set; }
        public string ImporterName { get; set; }
        public string ConsignorName { get; set; }
        public string ItemDescription { get; set; }
        public string BLNumber { get; set; }
        public string FlightNumber { get; set; }
        public string Shed { get; set; }
        public DateTime? BLDate { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? IGMDate { get; set; }
        public string IGMNumber { get; set; }
    }
}