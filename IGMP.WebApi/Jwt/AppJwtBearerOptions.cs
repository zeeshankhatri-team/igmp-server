using System.Text;
using System.Threading.Tasks;
using IGMP.Core;
using IGMP.WebApi.Auth.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace IGMP.WebApi.Jwt
{
    public class AppJwtBearerOptions
    {

        public AppJwtBearerOptions(JWTSettings settings)
        {
            this.Key = Encoding.ASCII.GetBytes(settings.Secret);
            this.Settings = settings;
        }

        private byte[] Key { get; set; }
        private JWTSettings Settings { get; set; }

        public void Options(JwtBearerOptions options)
        {
            options.Events = new JwtBearerEvents
            {
                OnTokenValidated = async context =>
                {
                    var manager = context.HttpContext.RequestServices.GetRequiredService<UserManager<AppUser>>();
                    var user = await manager.FindByNameAsync(context.Principal.Identity.Name);
                    if (user == null)
                    {
                        // return unauthorized if user no longer exists
                        context.Fail("Unauthorized");
                    }
                    else
                    {
                        var requestContext = context.HttpContext.RequestServices.GetRequiredService<IRequestContext>();
                        requestContext.UserName = user.UserName;
                        requestContext.UserId = user.Id;
                        context.Success();
                    }
                }
            };
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.ClaimsIssuer = this.Settings.Issuer;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(this.Key),
                //ValidIssuer = this.Settings.Issuer,
                ValidateIssuer = false,
                //ValidAudience = this.Settings.Audience,
                ValidateAudience = false,
                //RequireExpirationTime = false,
            };
        }
    }
}
