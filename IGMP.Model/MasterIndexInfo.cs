﻿using System;

namespace IGMP.Model
{
    [Info(HtmlTableId = "DataGrid11", FirstColumnIndex = 1)]
    public class MasterIndexInfo : BaseInfo
    {
        [InfoColumn(Index = -1, Caption = "Flight Info Id", DataType = typeof(int))]
        public int FlightInfoId { get; set; }

        [InfoColumn(Index = -2, Caption = "Flight Info")]
        public FlightInfo FlightInfo { get; set; }

        [InfoColumn(Index = -3, Caption = "Shed Ref Info Id", DataType = typeof(int))]
        public int ShedRefInfoId { get; set; }

        [InfoColumn(Index = -4, Caption = "Shed Ref Info")]
        public ShedRefInfo ShedRefInfo { get; set; }

        [InfoColumn(Index = -5, Caption = "IndexType")]
        public char IndexType { get; set; }

        [InfoColumn(Index = 0, Caption = "Index Number", DataType = typeof(int))]
        public int IndexNumber { get; set; }

        [InfoColumn(Index = 1, Caption = "BL Number")]
        public string BLNumber { get; set; }

        [InfoColumn(Index = 2, Caption = "BL Date", DataType = typeof(DateTime))]
        public DateTime? BLDate { get; set; }

        [InfoColumn(Index = 3, Caption = "Importer Name")]
        public string ImporterName { get; set; }

        [InfoColumn(Index = 4, Caption = "Importer Address")]
        public string ImporterAddress { get; set; }

        [InfoColumn(Index = 5, Caption = "Consignor Name")]
        public string ConsignorName { get; set; }

        [InfoColumn(Index = 6, Caption = "Weight", DataType = typeof(float))]
        public float Weight { get; set; }

        [InfoColumn(Index = 7, Caption = "Shed")]
        public string Shed { get; set; }
    }
}