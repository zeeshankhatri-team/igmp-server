using IGMP.Model;
using IGMP.Service.Model;
using System.Collections.Generic;

namespace IGMP.Service
{
    public interface INotificationService
    {
        PagedResult<SingleNotificationModel> GetAllSingle(PagedSorted<NotificationParam> parameters);
        PagedResult<ContinuousNotificationModel> GetAllContinuous(PagedSorted<NotificationParam> parameters);

        SingleNotificationModel GetSingle(int id);
        ContinuousNotificationModel GetContinuous(int id);

        void AddSingle(SingleNotificationModel model);
        void AddContinuous(ContinuousNotificationModel model);

        void UpdateSingle(int id, SingleNotificationModel model);
        void UpdateContinuous(int id, ContinuousNotificationModel model);

        void Delete(int id);
        void Archive(int id);

    }
}
