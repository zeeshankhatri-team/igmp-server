﻿using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IGMP.Data
{
    public class ShedRefInfoConfiguration : IEntityTypeConfiguration<ShedRefInfo>
    {
        public void Configure(EntityTypeBuilder<ShedRefInfo> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.HasOne(i => i.Port).WithMany().HasForeignKey(i => i.PortId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}