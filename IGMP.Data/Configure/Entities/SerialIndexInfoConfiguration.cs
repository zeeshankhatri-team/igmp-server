﻿using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IGMP.Data
{
    public class SerialIndexInfoConfiguration : IEntityTypeConfiguration<SerialIndexInfo>
    {
        public void Configure(EntityTypeBuilder<SerialIndexInfo> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.HasOne(i => i.Port).WithMany().HasForeignKey(i => i.PortId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(i => i.FlightInfo).WithMany().HasForeignKey(i => i.FlightInfoId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(i => i.MasterIndexInfo).WithMany().HasForeignKey(i => i.MasterIndexInfoId).OnDelete(DeleteBehavior.Restrict);
            // builder.HasOne(i => i.ConsoleIndexInfo).WithMany().HasForeignKey(i => i.ConsoleIndexInfoId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}