using Microsoft.EntityFrameworkCore;

namespace IGMP.Data
{
    public class UnitOfWork : IUnitOfWork {

        private AppDbContext Context { get; set; }

        public UnitOfWork (AppDbContext context) {
            this.Context = context;
        }

        public void Commit () {
            this.Context.SaveChanges ();
        }
    }
}