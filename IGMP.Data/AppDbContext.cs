﻿using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using IGMP.Data.Constants;
using IGMP.Core;

namespace IGMP.Data
{
    public class AppDbContext : DbContext {
        public AppDbContext (DbContextOptions<AppDbContext> options, IRequestContext requestContext) : base (options) {
            this.RequestContext = requestContext;
        }

        private IRequestContext RequestContext { get; set; }

        protected override void OnModelCreating (ModelBuilder builder) {

            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new FlightInfoConfiguration());
            builder.ApplyConfiguration(new ShedRefInfoConfiguration());
            builder.ApplyConfiguration(new MasterIndexInfoConfiguration());
            builder.ApplyConfiguration(new SerialIndexInfoConfiguration());
            builder.ApplyConfiguration(new NotificationConfiguration());

            builder.Query<ScrapingMetaInfo>().ToView(Views.ScrapingMetaInfos);
            builder.Query<IndexInfo>().ToView(Views.IndexInfos);

            // Filter Views
            builder.Query<Importer>().ToView(Views.Importers);
            builder.Query<Consignor>().ToView(Views.Consignors);
            builder.Query<Item>().ToView(Views.Items);
            builder.Query<BLNumber>().ToView(Views.BLNumbers);
            builder.Query<FlightNumber>().ToView(Views.FlightNumbers);
            builder.Query<Shed>().ToView(Views.Sheds);

            builder.HasSequence(Sequences.FlightInfoId);
            builder.HasSequence(Sequences.ShedRefInfoId);
            builder.HasSequence(Sequences.MasterIndexInfoId);
            builder.HasSequence(Sequences.SerialIndexInfoId);

            builder.Entity<Notification>().HasQueryFilter(n => n.UserId == this.RequestContext.UserId);
        }

        public int GetNextValue(string sequence)
        {
            SqlParameter result = new SqlParameter("@next", System.Data.SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            Database.ExecuteSqlCommand(
                       $"SELECT @next = (NEXT VALUE FOR dbo.{sequence})", result);

            return (int)result.Value;
        }

        public DbSet<FlightInfo> FlightInfos { get; set; }
        public DbSet<ShedRefInfo> ShedRefInfos { get; set; }
        public DbSet<MasterIndexInfo> MasterIndexInfos { get; set; }
        public DbSet<SerialIndexInfo> SerialIndexInfos { get; set; }

        public DbSet<Port> Ports { get; set; }

        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationExecutionDetail> NotificationExecutionDetails { get; set; }

        public DbQuery<ScrapingMetaInfo> ScrapingMetaInfos { get; set; }
        public DbQuery<IndexInfo> IndexInfos { get; set; }
        public DbQuery<Importer> Importers { get; set; }
        public DbQuery<Consignor> Consignors { get; set; }

    }
}