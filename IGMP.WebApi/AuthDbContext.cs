﻿
using IGMP.WebApi.Auth.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IGMP.WebApi
{
    public class AuthDbContext : IdentityDbContext<AppUser> {

        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base (options) {

        }

        protected override void OnModelCreating (ModelBuilder builder) {
            base.OnModelCreating(builder);
        }

    }
}