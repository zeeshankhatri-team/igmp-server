﻿using IGMP.Data.Constants;
using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace IGMP.Data.Initializer
{
    public static class AppDbInitializer
    {

        public static void Initialize(AppDbContext dbContext, ILogger logger)
        {
            if (dbContext.Database.EnsureCreated())
            {
                logger.LogInformation("Database created");

                var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scripts", "Views");
                var views = Views.GetViewsList();

                foreach (var view in views)
                {
                    var file = Path.GetFullPath($"{view}.sql", directory);
                    using (var reader = new StreamReader(file))
                    {
                        var command = reader.ReadToEnd();
                        dbContext.Database.ExecuteSqlCommand(command);
                    }
                }

                logger.LogInformation("Added Views");
            }

            dbContext.Database.Migrate();

            if(!dbContext.ShedRefInfos.Any())
            {
                dbContext.ShedRefInfos.Add(new ShedRefInfo() { Id = Defaults.NoShedId, Code = "", Name = "" });
            }
        }
    }
}
