﻿using System.Collections.Generic;

namespace IGMP.Scraping.Scrapers
{
    public interface IHandler
    {
        IDictionary<string, object> Handle(string response, IDictionary<string, object> parameters);
    }
}