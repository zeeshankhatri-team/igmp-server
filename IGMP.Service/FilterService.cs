using System;
using System.Collections.Generic;
using System.Linq;
using IGMP.Data;
using IGMP.Model;
using Microsoft.Extensions.DependencyInjection;

namespace IGMP.Service {
    public class FilterService : IFilterService {
        private IServiceProvider Provider { get; set; }

        public FilterService (IServiceProvider provider) {
            this.Provider = provider;
        }

        public IEnumerable<Importer> GetImporters (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<Importer>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }

        public IEnumerable<Consignor> GetConsignors (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<Consignor>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }

        public IEnumerable<Item> GetItems (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<Item>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }

        public IEnumerable<BLNumber> GetBLNumbers (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<BLNumber>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }

        public IEnumerable<FlightNumber> GetFlightNumbers (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<FlightNumber>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }

        public IEnumerable<Shed> GetSheds (string filter) {
            var repository = this.Provider.GetRequiredService<IViewRepository<Shed>> ();
            var list = repository.Find (c => c.Name.Contains (filter)).ToList ();
            return list;
        }
    }
}