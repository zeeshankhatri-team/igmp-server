using System;
using System.Collections.Generic;

namespace IGMP.Model {

    public class Notification
    {
        public Notification()
        {
            this.NotificationExecutionDetails = new HashSet<NotificationExecutionDetail>();
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public NotificationType NotificationType { get; set; }
        public string ImporterName { get; set; }
        public string ConsignorName { get; set; }
        public string ItemDescription { get; set; }
        public string BLNumber { get; set; }

        public DateTime? LastExecutedOn { get; set; }
        public int ExecutionResultCount { get; set; }

        public bool Archived { get; set; }

        public ICollection<NotificationExecutionDetail> NotificationExecutionDetails { get; private set; }
    }

    public class NotificationExecutionDetail
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public Notification Notification { get; set; }

        public DateTime ExecutedOn { get; set; }
        public int ResultCount { get; set; }
    }

    public enum NotificationType
    {
        Single = 1,
        Continuous = 2,
    }
}