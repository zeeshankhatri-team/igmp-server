﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using IGMP.Data;

namespace IGMP.Scraping.Scrapers
{
    public class HomeScraper : IHomeScraper
    {
        private IServiceProvider ServiceProvider { get; set; }
        private IConfiguration Configuration { get; set; }
        private ILogger<HomeScraper> Logger { get; set; }
        private AppDbContext DbContext { get; set; }

        public int RetryLimit { get; set; }

        public HomeScraper(
            IServiceProvider serviceProvider,
            AppDbContext dbContext,
            IConfiguration configuration, 
            ILogger<HomeScraper> logger)
        {
            this.ServiceProvider = serviceProvider;
            this.Configuration = configuration;
            this.Logger = logger;

            this.RetryLimit = configuration.GetSection("ScrapService").GetValue<int>("RetryLimit");
        }

        public IDictionary<string, object> Scrap(IDictionary<string, object> dict)
        {           
            var request = ServiceProvider.GetService<IHomeRequest>();
            var handler = ServiceProvider.GetService<IHomeHandler>();
            var retry = 0;

            while (retry <= this.RetryLimit)
            {

                string retryText = Helper.GetRetryText(retry);
                Logger.LogInformation($"Scraping Home{retryText}");
                try
                {
                    var response = request.Send(null);
                    var result = handler.Handle(response, dict);
                    Logger.LogInformation($"Scraping Home: Succeeded{retryText}");
                    return result;
                }
                catch (Exception ex)
                {
                    var message = $"Scraping for Home: FAILED{retryText}";
                    Logger.LogInformation(message);
                    Logger.LogDebug(ex, message);
                    retry++;
                    Task.Delay(2000).Wait();
                }
            }
            return null;
        }
    }
}
