﻿using IGMP.Service;
using IGMP.Service.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGMP.WebApi.Controllers
{
    [Route ("api/[controller]")]
    [Authorize]
    [ApiController]
    public class SearchController : ControllerBase {
        public SearchController (
            ISearchService service
        ) {
            this.Service = service;
        }

        public ISearchService Service { get; private set; }

        [HttpPost]
        public ActionResult<PagedResult<SearchItem>> Post ([FromBody] PagedSorted<SearchParam> parameters) {
            try {
                return this.Ok (this.Service.Search (parameters));
            } catch (WebApiAuthException ex) {
                return this.BadRequest (ex.Message);
            }

        }
    }
}