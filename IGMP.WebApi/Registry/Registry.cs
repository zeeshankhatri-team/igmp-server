﻿using System;
using IGMP.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IGMP.WebApi.Jwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using IGMP.WebApi.Auth.Model;
using IGMP.WebApi.Services;

namespace IGMP.WebApi
{
    public static class Registry
    {
        public static void RegisterWebApi(this IServiceCollection services, IConfiguration configuration)
        {
            // configure strongly typed settings objects
            var jwtSection = configuration.GetSection("JWTSettings");
            services.Configure<JWTSettings>(jwtSection);

            var emailSection = configuration.GetSection("EmailSettings");
            services.Configure<EmailSettings>(emailSection);

            var smtpSection = configuration.GetSection("SmtpSettings");
            services.Configure<SmtpSettings>(smtpSection);
            

            services.AddDbContext<AuthDbContext>(options => {
                options.UseSqlServer(configuration.GetConnectionString("AuthDB"));
            }, ServiceLifetime.Scoped);

            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<AuthDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 0;

                options.SignIn.RequireConfirmedEmail = true;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            });

            // configure jwt authentication
            var jwtSettings = jwtSection.Get<JWTSettings>();
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(configureOptions: new AppJwtBearerOptions(jwtSettings).Options);

            services.AddScoped<IEmailService, SmtpService>();
        }

    }
}
