﻿using System;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using IGMP.Core;
using Microsoft.AspNetCore.Identity;
using IGMP.WebApi.Auth.Model;
using System.Threading.Tasks;
using IGMP.WebApi.Model;
using Microsoft.Extensions.DependencyInjection;

namespace IGMP.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/profile")]
    public class ProfileController : ControllerBase
    {
        private UserManager<AppUser> Manager { get; set; }
        private IMapper Mapper { get; set; }
        private JWTSettings JWTSettings { get; set; }
        private IServiceProvider Provider { get; set; }

        public ProfileController(
            IServiceProvider provider,
            UserManager<AppUser> manager,
            IMapper mapper)
        {
            Provider = provider;
            Manager = manager;
            Mapper = mapper;
        }
      
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var context = this.Provider.GetRequiredService<IRequestContext>();
            var user = await this.Manager.FindByNameAsync(context.UserName);

            var model = this.Mapper.Map<UserProfileModel>(user);
            return Ok(model);
        }

    }
}
