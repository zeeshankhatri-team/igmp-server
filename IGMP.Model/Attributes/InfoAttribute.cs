﻿using System;

namespace IGMP.Model
{
    public class InfoAttribute : Attribute
    {
        public string HtmlTableId { get; set; }
        public int FirstColumnIndex { get; set; }
    }

}