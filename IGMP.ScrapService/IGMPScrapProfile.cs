using AutoMapper;
using IGMP.Model;

namespace IGMP.WebApi {
    public class IGMPScrapProfile : Profile {
        public IGMPScrapProfile () {
            CreateMap<ConsoleIndexInfo, MasterIndexInfo>();
        }
    }
}