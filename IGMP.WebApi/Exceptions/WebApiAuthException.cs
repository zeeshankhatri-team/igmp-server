using System;
using System.Runtime.Serialization;

namespace IGMP.WebApi
{
    [Serializable]
    public class WebApiAuthException : Exception
    {
        public WebApiAuthException()
        {
        }

        public WebApiAuthException(string message) : base(message)
        {
        }

        public WebApiAuthException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WebApiAuthException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}