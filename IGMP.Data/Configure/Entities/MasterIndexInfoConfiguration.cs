﻿using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IGMP.Data
{
    public class MasterIndexInfoConfiguration : IEntityTypeConfiguration<MasterIndexInfo>
    {
        public void Configure(EntityTypeBuilder<MasterIndexInfo> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.HasOne(i => i.Port).WithMany().HasForeignKey(i => i.PortId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(i => i.FlightInfo).WithMany().HasForeignKey(i => i.FlightInfoId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(i => i.ShedRefInfo).WithMany().HasForeignKey(i => i.ShedRefInfoId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}