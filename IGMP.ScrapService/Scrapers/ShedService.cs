﻿using IGMP.Data;
using IGMP.Model;
using System.Collections.Generic;
using System.Linq;
using IGMP.Data.Constants;

namespace IGMP.Scraping.Scrapers
{
    public class ShedService
    {
        private AppDbContext DbContext { get; set; }
        public IList<ShedRefInfo> Sheds { get; private set; }

        public ShedService(AppDbContext dbContext)
        {
            this.DbContext = dbContext;
            this.Sheds = dbContext.ShedRefInfos.ToList();
        }

        public int GetShedRefInfoId(string shedWS)
        {
            int result = Defaults.NoShedId;
            
            if (!string.IsNullOrWhiteSpace(shedWS))
            {
                ShedRefInfo shed = null;
                int sourceWebId;
                bool isNumeric = int.TryParse(shedWS, out sourceWebId);

                if (isNumeric)
                {
                    shed = this.Sheds.FirstOrDefault(i => i.SourceWebId == sourceWebId);
                }
                else
                {
                    shed = this.Sheds.FirstOrDefault(i => i.SourceWebDesc == shedWS);
                }

                if (shed != null)
                {
                    result = shed.Id;
                }
            }

            return result;
        }

        public void Extract(IList<ShedRefInfo> shedsWS, Port port, int year, int number)
        {
            var newShedsWS = shedsWS.Where(s => !this.Sheds.Any(e => e.SourceWebId == s.SourceWebId && e.SourceWebDesc == s.SourceWebDesc));

            if (newShedsWS.Count() > 0)
            {
                foreach (var shedWS in newShedsWS)
                {
                    var shed = new ShedRefInfo() {
                        Code = shedWS.SourceWebDesc.Trim(),
                        Name = shedWS.SourceWebDesc.Trim(),
                        SourceWebId = shedWS.SourceWebId,
                        SourceWebDesc = shedWS.SourceWebDesc
                    };
                    AddShed(shed, port, year, number);
                }

                this.DbContext.SaveChanges();
            }
        }

        public void Extract(IEnumerable<string> shedsWS, Port port, int year, int number)
        {
            bool newFound = false;

            foreach (var shedWS in shedsWS)
            {
                if(!string.IsNullOrWhiteSpace(shedWS))
                {               
                    int sourceWebId;
                    bool isNumeric = int.TryParse(shedWS, out sourceWebId);
                    if (isNumeric)
                    {
                        if (!this.Sheds.Any(e => e.SourceWebId == sourceWebId))
                        {
                            var shed = new ShedRefInfo()
                            {
                                Code = sourceWebId.ToString(),
                                Name = sourceWebId.ToString(),
                                SourceWebId = sourceWebId,
                            };
                            AddShed(shed, port, year, number);
                            newFound = true;
                        }
                    }
                    else
                    {
                        if (!this.Sheds.Any(e => e.SourceWebDesc == shedWS))
                        {
                            var shed = new ShedRefInfo()
                            {
                                Code = shedWS.ToString(),
                                Name = shedWS.ToString(),
                                SourceWebDesc = shedWS,
                            };
                            AddShed(shed, port, year, number);
                            newFound = true;
                        }
                    }
                }
            }

            if (newFound)
            {
                this.DbContext.SaveChanges();
            }
        }

        private void AddShed(ShedRefInfo shed, Port port, int year, int number)
        {
            shed.Id = this.DbContext.GetNextValue(Sequences.ShedRefInfoId);
            shed.PortId = port.Id;
            shed.PortWebSourceCode = port.SourceWebCode;
            shed.Year = year;
            shed.Number = number;
            this.Sheds.Add(shed);
            this.DbContext.ShedRefInfos.Add(shed);
        }
    }
}
