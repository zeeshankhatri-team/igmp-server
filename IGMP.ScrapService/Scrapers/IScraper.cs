﻿using System.Collections.Generic;

namespace IGMP.Scraping.Scrapers
{
    public interface IScraper
    {
        IDictionary<string, object> Scrap(IDictionary<string, object> dict);
    }
}
