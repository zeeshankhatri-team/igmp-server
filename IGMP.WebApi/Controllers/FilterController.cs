﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using IGMP.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGMP.WebApi.Controllers
{
    [Route ("api/[controller]")]
    [Authorize]
    [ApiController]
    public class FilterController : ControllerBase {
        public FilterController (
            IFilterService service,
            IMapper mapper
        ) {
            this.Service = service;
            this.Mapper = mapper;
        }

        public IFilterService Service { get; private set; }
        public IMapper Mapper { get; private set; }

        [HttpGet]
        [Route("Importers")]
        public ActionResult<IEnumerable<string>> GetImporters (string filter) {
            try {
                return this.Ok (this.Service.GetImporters(filter).Select(i => i.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Consignors")]
        public ActionResult<IEnumerable<string>> GetConsignors (string filter) {
            try {
                return this.Ok (this.Service.GetConsignors(filter).Select(c => c.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Items")]
        public ActionResult<IEnumerable<string>> GetItems (string filter) {
            try {
                return this.Ok (this.Service.GetItems(filter).Select(c => c.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("BLNumbers")]
        public ActionResult<IEnumerable<string>> GetBLNumbers (string filter) {
            try {
                return this.Ok (this.Service.GetBLNumbers(filter).Select(c => c.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("FlightNumbers")]
        public ActionResult<IEnumerable<string>> GetFlightNumbers (string filter) {
            try {
                return this.Ok (this.Service.GetFlightNumbers(filter).Select(c => c.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Sheds")]
        public ActionResult<IEnumerable<string>> GetSheds (string filter) {
            try {
                return this.Ok (this.Service.GetSheds(filter).Select(c => c.Name));
            } catch (WebApiAuthException ex) {
                return this.BadRequest(ex.Message);
            }
        }

    }
}