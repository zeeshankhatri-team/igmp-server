﻿namespace IGMP.WebApi.Services
{

    public class EmailSettings
    {
        public string FromEmail { get; set; }
        public string FromName { get; set; }
    }
}
