using System.Collections.Generic;

namespace IGMP.Service.Model
{
    public class PagedResult<T>
    {
        public int Length { get; set; }
        public IEnumerable<T> Items { get; set; }

        public PagedResult(IEnumerable<T> items, int length)
        {
            this.Items = items;
            this.Length = length;
        }
    }
}