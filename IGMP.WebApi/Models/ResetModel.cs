﻿namespace IGMP.WebApi.Model
{
    public class ResetModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}