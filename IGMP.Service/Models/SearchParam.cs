﻿using System;

namespace IGMP.Service.Model {
    public class SearchParam {
        public string ImporterName { get; set; }
        public string ConsignorName { get; set; }
        public string ItemDescription { get; set; }
        public string BLNumber { get; set; }
        public string FlightNumber { get; set; }
        public string Shed { get; set; }
        public DateTime? BLDateFrom { get; set; }
        public DateTime? BLDateTo { get; set; }
        public DateTime? ArrivalDateFrom { get; set; }
        public DateTime? ArrivalDateTo { get; set; }
        public DateTime? IGMDateFrom { get; set; }
        public DateTime? IGMDateTo { get; set; }
        public int? IGMNumber { get; set; }
        public int? IndexNumber { get; set; }
    }
}