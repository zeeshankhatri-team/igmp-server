namespace IGMP.Service.Constants
{
    public class SortDirection
    {
        public const string Ascending = "asc";
        public const string Descending = "desc";
    }
}