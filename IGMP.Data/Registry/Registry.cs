﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IGMP.Data
{
    public static class Registry
    {
        public static void RegisterData(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext> (options => {
                options.UseSqlServer (configuration.GetConnectionString ("AppDB"));
                options.UseQueryTrackingBehavior (QueryTrackingBehavior.NoTracking);
            }, ServiceLifetime.Scoped);

            services.AddScoped<IUnitOfWork, UnitOfWork>();            
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IViewRepository<>), typeof(ViewRepository<>));
        }
    }
}
