﻿namespace IGMP.Scraping
{
    using System.IO;
    using System;
    using System.Collections.Generic;

    public class CsvAdapter : ICsvAdapter
    {
        public void Append<T>(string directory, IList<T> list)
        {
            var fileName = $"{typeof(T).Name}.csv";
            var path = Path.Combine(directory, fileName);

            CreateDirectory(directory);

            using (var writer = File.AppendText(path))
            {
                var csv = new CsvHelper.CsvWriter(writer);
                csv.WriteRecords(list);
            }

        }

        public void Create<T>(string directory)
        {
            var fileName = $"{typeof(T).Name}.csv";
            var path = Path.Combine(directory, fileName);

            CreateDirectory(directory);

            using (var writer = File.CreateText(path))
            {
                var csv = new CsvHelper.CsvWriter(writer);
                csv.WriteHeader<T>();
            }
        }

        private static void CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                try
                {
                    Directory.CreateDirectory(directory);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}