﻿using IGMP.Model;
using System;

namespace IGMP.Service.Model {

    public abstract class NotificationModel
    {
        public int Id { get; set; }
        public NotificationType NotificationType { get; set; }

        public DateTime? LastExecutedOn { get; set; }
        public int ExecutionResultCount { get; set; }
    }

    public class ContinuousNotificationModel : NotificationModel
    {
        public string ImporterName { get; set; }
        public string ConsignorName { get; set; }
        public string ItemDescription { get; set; }
    }

    public class SingleNotificationModel : NotificationModel
    {
        public string BLNumber { get; set; }
    }

    public class NotificationParam
    {
        public bool Archived { get; set; }
    }
}