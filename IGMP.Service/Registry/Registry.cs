﻿
using Microsoft.Extensions.DependencyInjection;

namespace IGMP.Service
{
    public static class Registry
    {
        public static void RegisterService(this IServiceCollection services)
        {
            services.AddScoped<ISearchService, SearchService>();   
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IFilterService, FilterService>();         
        }
    }
}
