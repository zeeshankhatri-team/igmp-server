using IGMP.Service.Model;

namespace IGMP.Service
{
    public interface ISearchService
    {
        PagedResult<SearchItem> Search(PagedSorted<SearchParam> parameters);
    }
}
