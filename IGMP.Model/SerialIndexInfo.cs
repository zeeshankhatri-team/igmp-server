﻿namespace IGMP.Model
{
    [Info(HtmlTableId = "MyDataGrid2", FirstColumnIndex = 1)]
    public class SerialIndexInfo : BaseInfo
    {
        [InfoColumn(Index = -1, Caption = "Flight Info Id", DataType = typeof(int))]
        public int FlightInfoId { get; set; }

        [InfoColumn(Index = -2, Caption = "Master Index Info")]
        public FlightInfo FlightInfo { get; set; }

        [InfoColumn(Index = -3, Caption = "Master Index Info Id", DataType = typeof(int))]
        public int? MasterIndexInfoId { get; set; }

        [InfoColumn(Index = -3, Caption = "Master Index Info")]
        public MasterIndexInfo MasterIndexInfo { get; set; }

        // [InfoColumn(Index = -4, Caption = "Console Index Info Id", DataType = typeof(int))]
        // public int? ConsoleIndexInfoId { get; set; }

        // [InfoColumn(Index = -5, Caption = "Console Index Info")]
        // public ConsoleIndexInfo ConsoleIndexInfo { get; set; }

        [InfoColumn(Index = 0, Caption = "Index Number", DataType = typeof(int))]
        public int IndexNumber { get; set; }

        [InfoColumn(Index = 1, Caption = "Index Serial Number", DataType = typeof(int))]
        public int IndexSerialNumber { get; set; }

        [InfoColumn(Index = 2, Caption = "Item Description")]
        public string ItemDescription { get; set; }

        [InfoColumn(Index = 3, Caption = "Quantity", DataType = typeof(float))]
        public float Quantity { get; set; }

        [InfoColumn(Index = 4, Caption = "Unit")]
        public string Unit { get; set; }

        [InfoColumn(Index = 5, Caption = "Packages")]
        public string Packages { get; set; }

        [InfoColumn(Index = 6, Caption = "Packages Unit")]
        public string PackagesUnit { get; set; }
    }
}