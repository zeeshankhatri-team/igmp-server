﻿namespace IGMP.Scraping
{
    public interface IExecutionContext
    {
        string Session { get; }
        string GetSessionDirectory();
        void StartSession();
        void EndSession();  
    }
}
