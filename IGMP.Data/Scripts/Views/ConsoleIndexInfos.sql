﻿-- CREATE OR ALTER VIEW [dbo].[View_ConsoleIndexInfos]
-- AS
-- SELECT			S.Id, S.PortId, S.Year, S.IndexNumber, S.IndexSerialNumber, S.ItemDescription, S.Number AS IGMNumber, F.IGMDate, S.FlightInfoId, S.ConsoleIndexInfoId AS IndexInfoId, 'C' AS IndexType,
-- 						P.Name AS PortName, P.Code AS PortCode, P.SourceWebCode AS PortSourceWebCode, 
-- 						F.FlightNumber, F.AirLine, F.ArrivalDate, 
-- 						C.ImporterName, C.ConsignorName, C.BLNumber, C.BLDate, 
-- 						CAST(NULL AS nvarchar) AS Shed,
-- 						CAST(NULL AS nvarchar) AS ShedCode, 
-- 						CAST(NULL AS nvarchar) AS ShedName, 
-- 						CAST(NULL AS int) AS ShedSourceWebId, 
-- 						CAST(NULL AS nvarchar) AS ShedSourceWebDesc
-- FROM            dbo.SerialIndexInfos AS S
-- 					INNER JOIN dbo.Ports AS P ON S.PortId = P.Id
-- 					INNER JOIN dbo.FlightInfos AS F ON S.FlightInfoId = F.Id 
-- 					INNER JOIN dbo.ConsoleIndexInfos AS C ON S.ConsoleIndexInfoId = C.Id
					