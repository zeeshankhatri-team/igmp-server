using AutoMapper;
using IGMP.Model;
using IGMP.Service.Model;

namespace IGMP.Service {
    public class IGMPProfile : Profile {
        public IGMPProfile () {
            CreateMap<IndexInfo, SearchItem> ();
            CreateMap<Notification, NotificationModel>();
            CreateMap<Notification, ContinuousNotificationModel>();
            CreateMap<Notification, SingleNotificationModel>();
            CreateMap<ContinuousNotificationModel, Notification>();
            CreateMap<SingleNotificationModel, Notification>();
        }
    }
}