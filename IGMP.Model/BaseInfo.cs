﻿namespace IGMP.Model
{
    public abstract class BaseInfo
    {
        [InfoColumn(Index = -100, Caption = "Id")]
        public int Id { get; set; }

        [InfoColumn(Index = -50, Caption = "Port Id")]
        public int PortId { get; set; }

        [InfoColumn(Index = -40, Caption = "Port")]
        public Port Port { get; set; }

        [InfoColumn(Index = -30, Caption = "Port (Web Source Code)")]
        public string PortWebSourceCode { get; set; }

        [InfoColumn(Index = -20, Caption = "IGM Number", DataType = typeof(int))]
        public int Number { get; set; }

        [InfoColumn(Index = -10, Caption = "Year", DataType = typeof(int))]
        public int Year { get; set; }
    }
}