﻿namespace IGMP.WebApi.Model
{
    public class VerifyModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }
}