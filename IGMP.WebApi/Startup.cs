﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using IGMP.Core;
using IGMP.Model;
using IGMP.Data;
using IGMP.Service;
using AutoMapper;

namespace IGMP.WebApi
{
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddAutoMapper();
            services.AddMvc ().SetCompatibilityVersion (CompatibilityVersion.Version_2_2);
            services.AddCors (options => options.AddPolicy ("AllowAllHeadersAndMethods",
                builder => {
                    builder.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader ()
                        .AllowAnyMethod ();
                }));

            services.RegisterCore();
            services.RegisterModel();
            services.RegisterData(Configuration);
            services.RegisterService();
            services.RegisterWebApi(Configuration);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
                app.UseCors ("AllowAllHeadersAndMethods");
            } else {
                // app.UseHsts ();
            }

            // app.UseHttpsRedirection ();
            app.UseAuthentication();
            app.UseMvc ();
            app.UseDefaultFiles();
            app.UseStaticFiles();

        }
    }
}