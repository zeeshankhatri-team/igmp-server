﻿namespace IGMP.Scraping.Scrapers
{
    using System.IO;
    using HtmlAgilityPack;
    using System.Linq;
    using IGMP.Model;
    using System.Collections.Generic;
    using IGMP.Scraping.Constants;
    using IGMP.Data;

    public class HomeHandler : IHomeHandler
    {
        private HtmlDocument Document { get; set; }
        private IExecutionContext Context { get; set; }
        private AppDbContext DbContext { get; set; }
        private ICsvAdapter CsvAdapter { get; set; }
        private IList<Port> Ports { get; set; }

        public HomeHandler(
            IExecutionContext appContext,
            AppDbContext dbContext,
            ICsvAdapter csvAdapter)
        {
            this.Context = appContext;
            this.CsvAdapter = csvAdapter;
            this.DbContext = dbContext;
            this.Document = new HtmlDocument();
        }

        public IDictionary<string, object> Handle(string response, IDictionary<string, object> parameters)
        {
            this.Document.LoadHtml(response);
            this.Populate(parameters);

            var viewstate = this.Document.GetElementbyId("__VIEWSTATE").Attributes["value"].Value;
            var eventvalidation = this.Document.GetElementbyId("__EVENTVALIDATION").Attributes["value"].Value;
            var portsWS = this.Document.GetElementbyId("PORTCD").Elements("option").Select(p => new { Code = p.Attributes["value"].Value, Name = p.InnerText } );

            var newPortsWS = portsWS.Where(p => !this.Ports.Any(e => e.SourceWebCode == p.Code));
            
            if(newPortsWS.Count() > 0)
            {
                foreach (var portWS in newPortsWS)
                {
                    var port = new Port() { Code = portWS.Code.Trim(), Name = portWS.Name.Trim(), SourceWebCode = portWS.Code, SourceWebName = portWS.Name };
                    this.DbContext.Ports.Add(port);
                    this.Ports.Add(port);
                }

                this.DbContext.SaveChangesAsync();
                this.MakeCsvs(this.Ports);
            }
            
            var dict = new Dictionary<string, object>();
            dict.Add(ScrapingParams.ViewState, viewstate);
            dict.Add(ScrapingParams.EventValidation, eventvalidation);
            dict.Add(ScrapingParams.Ports, this.Ports);
           
            return dict;
        }

        private void Populate(IDictionary<string, object> parameters)
        {
            this.Ports = (IList<Port>)parameters[ScrapingParams.Ports];
        }

        private void MakeCsvs(IList<Port> ports)
        {
            var directory = Path.Combine(Context.GetSessionDirectory());

            CsvAdapter.Append(directory, ports);
        }
    }

}