﻿namespace IGMP.Core
{
    public interface IRequestContext
    {
        string UserId { get; set; }
        string UserName { get; set; }
    }

    public class RequestContext : IRequestContext
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}