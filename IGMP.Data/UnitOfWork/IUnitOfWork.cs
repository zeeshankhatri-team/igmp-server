namespace IGMP.Data
{
    public interface IUnitOfWork {
        void Commit ();
    }
}