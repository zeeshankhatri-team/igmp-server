﻿using System.Collections.Generic;
using System.Linq;

namespace IGMP.Data.Constants
{
    public static class Views
    {
        public const string ScrapingMetaInfos = "View_ScrapingMetaInfos";
        public const string MasterIndexInfos = "View_MasterIndexInfos";
        // public const string ConsoleIndexInfos = "View_ConsoleIndexInfos";
        public const string IndexInfos = "View_IndexInfos";
        public const string Importers = "View_Importers";
        public const string Consignors = "View_Consignors";
        public const string Items = "View_Items";
        public const string BLNumbers = "View_BLNumbers";
        public const string FlightNumbers = "View_FlightNumbers";
        public const string Sheds = "View_Sheds";


        public static IEnumerable<string> GetViewsList()
        {
            return typeof(Views).GetFields().Select(f => f.Name);
        }
    }
}
