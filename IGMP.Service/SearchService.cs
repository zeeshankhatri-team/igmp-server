﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using IGMP.Data;
using IGMP.Model;
using IGMP.Service.Constants;
using IGMP.Service.Extensions;
using IGMP.Service.Helpers;
using IGMP.Service.Model;

namespace IGMP.Service
{
    public class SearchService : ISearchService {
        public SearchService (IViewRepository<IndexInfo> repository, IMapper mapper) {
            this.Repository = repository;
            this.Mapper = mapper;
        }

        public IMapper Mapper { get; private set; }

        public IViewRepository<IndexInfo> Repository { get; }

        public PagedResult<SearchItem> Search (PagedSorted<SearchParam> pagedSorted) {
            var parameters = pagedSorted.Parameters;
            if(parameters != null)
            {
                var query = this.Repository.AsQueryable();

                if (!string.IsNullOrWhiteSpace (parameters.ImporterName)) {
                    query = query.Where (e => e.ImporterName.Contains (parameters.ImporterName.Trim()));
                }

                if (!string.IsNullOrWhiteSpace (parameters.ConsignorName)) {
                    query = query.Where (e => e.ConsignorName.Contains (parameters.ConsignorName.Trim()));
                }

                if (!string.IsNullOrWhiteSpace (parameters.ItemDescription)) {
                    query = query.Where (e => e.ItemDescription.Contains (parameters.ItemDescription.Trim()));
                }

                if (!string.IsNullOrWhiteSpace (parameters.BLNumber)) {
                    query = query.Where (e => e.BLNumber.Contains (parameters.BLNumber.Trim()));
                }

                if (!string.IsNullOrWhiteSpace (parameters.FlightNumber)) {
                    query = query.Where (e => e.FlightNumber.Contains (parameters.FlightNumber.Trim()));
                }

                if (!string.IsNullOrWhiteSpace (parameters.Shed)) {
                    query = query.Where (e => e.ShedName.Contains (parameters.Shed.Trim()));
                }

                if (parameters.BLDateFrom != null && parameters.BLDateTo != null) {
                    query = query.Where (e => e.BLDate.Value.Date >= parameters.BLDateFrom && e.BLDate.Value.Date <= parameters.BLDateTo);
                }

                if (parameters.ArrivalDateFrom != null && parameters.ArrivalDateTo != null) {
                    query = query.Where (e => e.ArrivalDate.Value.Date >= parameters.ArrivalDateFrom && e.ArrivalDate.Value.Date <= parameters.ArrivalDateTo);
                }

                if (parameters.IGMDateFrom != null && parameters.IGMDateTo != null) {
                    query = query.Where (e => e.IGMDate.Value.Date >= parameters.IGMDateFrom && e.IGMDate.Value.Date <= parameters.IGMDateTo);
                }

                if (parameters.IGMNumber != null) {
                    query = query.Where (e => e.IGMNumber == parameters.IGMNumber.Value);
                }

                if (parameters.IndexNumber != null)
                {
                    query = query.Where(e => e.IndexNumber == parameters.IndexNumber.Value);
                }

                var wf = new WhereFinder ();
                var wheres = wf.GetWhere (query.Expression);

                if (!wheres.Any ()) {
                    throw new ServiceException ("Empty search request.");
                }

                if(!string.IsNullOrWhiteSpace(pagedSorted.SortBy))
                {
                    query = query.OrderByField(pagedSorted.SortBy, pagedSorted.Direction != SortDirection.Descending);
                }
                
                var length = query.Count();
                var list = query.Skip(pagedSorted.PageIndex * pagedSorted.PageSize).Take(pagedSorted.PageSize).ToList();
                var result = this.Mapper.Map<IEnumerable<SearchItem>> (list);
                var pagedResult = new PagedResult<SearchItem> (result, length);
                return pagedResult;
            }
            else
            {
                throw new ServiceException ("Empty search request.");
            }
        }
    }
}