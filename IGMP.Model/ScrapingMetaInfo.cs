﻿namespace IGMP.Model
{
    public class ScrapingMetaInfo
    {
        public int PortId { get; set; }
        public int Year { get; set; }
        public int Number { get; set; }
    }
}