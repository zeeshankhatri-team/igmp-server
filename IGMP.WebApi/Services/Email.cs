﻿namespace IGMP.WebApi.Services
{
    public class Email
    {
        public string To { get; internal set; }
        public string Subject { get; internal set; }
        public string Body { get; internal set; }
    }
}
