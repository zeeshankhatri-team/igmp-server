﻿using AutoMapper;
using IGMP.Service;
using IGMP.Service.Model;
using IGMP.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IGMP.WebApi.Controllers
{
    [Route ("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ContinuousNotificationsController : ControllerBase {
        public ContinuousNotificationsController(
            INotificationService service,
            IMapper mapper
        ) {
            this.Service = service;
            this.Mapper = mapper;
        }

        private IMapper Mapper { get; set; }
        public INotificationService Service { get; private set; }

        [HttpGet]
        public ActionResult<PagedResult<ContinuousNotificationModel>> Get([FromQuery] PagingSortingParam queryParams)
        {
            var pagedSorted = queryParams.ToPagedSorted<NotificationParam>();
            pagedSorted.Parameters = new NotificationParam() { Archived = false };
            var results = this.Service.GetAllContinuous(pagedSorted);
            return Ok(results);
        }

        [HttpGet("Archived")]
        public ActionResult<PagedResult<ContinuousNotificationModel>> GetArchived([FromQuery] PagingSortingParam queryParams)
        {
            var pagedSorted = queryParams.ToPagedSorted<NotificationParam>();
            pagedSorted.Parameters = new NotificationParam() { Archived = false };
            var results = this.Service.GetAllContinuous(pagedSorted);
            return Ok(results);
        }

        [HttpGet("{id}")]
        public ActionResult<ContinuousNotificationModel> Get(int id)
        {
            var result = this.Service.GetContinuous(id);
            return Ok(result);
        }

        [HttpPost]
        public void Post([FromBody] ContinuousNotificationModel model)
        {
            this.Service.AddContinuous(model);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ContinuousNotificationModel model)
        {
            this.Service.UpdateContinuous(id, model);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.Service.Delete(id);
        }
    }
}