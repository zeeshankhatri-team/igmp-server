﻿using System.Collections.Generic;

namespace IGMP.Scraping
{
    public interface ICsvAdapter
    {
        void Create<T>(string directory);
        void Append<T>(string directory, IList<T> list);
    }
}