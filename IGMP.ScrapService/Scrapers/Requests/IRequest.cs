﻿using System.Collections.Generic;

namespace IGMP.Scraping.Scrapers
{
    public interface IRequest
    {
        string Send(IDictionary<string, object> parameters);
    }
}