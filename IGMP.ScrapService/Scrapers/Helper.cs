﻿namespace IGMP.Scraping.Scrapers
{
    public static class Helper
    {
        public static string GetRetryText(int retry)
        {
            var text = string.Empty;
            if (retry > 0) text = $" (Retry {retry})";
            return text;
        }
    }
}
