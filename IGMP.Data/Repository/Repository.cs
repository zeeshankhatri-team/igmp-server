using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace IGMP.Data {
    public class Repository<T> : IRepository<T> where T : class {
        private DbSet<T> DbSet { get; set; }
        private DbContext DbContext { get; set; }

        public Repository (AppDbContext context) {
            this.DbContext = context;
            this.DbSet = context.Set<T> ();
        }

        public IQueryable<T> AsQueryable () {
            return this.DbSet.AsNoTracking ();
        }

        public IEnumerable<T> GetAll (params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();            
            return PerformInclusions (includeProperties, query);
        }

        public int Count()
        {
            return AsQueryable().Count();
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return AsQueryable().Count(predicate);
        }

        public IEnumerable<T> Find (Expression<Func<T, bool>> where,
            params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();
            query = PerformInclusions (includeProperties, query);
            return query.Where (where);
        }

        public T Single (Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();
            query = PerformInclusions (includeProperties, query);
            return query.Single (where);
        }

        public T SingleOrDefault (Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();
            query = PerformInclusions (includeProperties, query);
            return query.SingleOrDefault (where);
        }

        public T First (Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();
            query = PerformInclusions (includeProperties, query);
            return query.First (where);
        }

        public T FirstOrDefault (Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = AsQueryable ();
            query = PerformInclusions (includeProperties, query);
            return query.FirstOrDefault (where);
        }

        public void Remove (T entity) {
            this.DbSet.Remove (entity);
        }

        public void RemoveRange (IEnumerable<T> entities) {
            this.DbSet.RemoveRange (entities);
        }

        public void Add (T entity) {
            this.DbSet.Add (entity);
        }

        public void AddRange (IEnumerable<T> entities) {
            this.DbSet.AddRange (entities);
        }

        public void Update (T entity) {
            this.DbSet.Attach (entity).State = EntityState.Modified;
        }

        public void UpdateRange (IEnumerable<T> entities) {
            // TODO have to find out a way... will not update as it is not marked as modified
            this.DbSet.AttachRange (entities);
            foreach(var entity in entities)
            {
                this.DbContext.Entry(entity).State = EntityState.Modified;
            }
        }

        public void Update (T entity, params string[] changedPropertyNames) {
            var entry = this.DbSet.Attach (entity);
            foreach (var property in changedPropertyNames) {
                entry.Property (property).IsModified = true;
            }
        }

        public void UpdateRange (IEnumerable<T> entities, params string[] changedPropertyNames) {
            // TODO have to find out a way... will not update as it is not marked as modified
            this.DbSet.AttachRange (entities);
            foreach (var entity in entities) {
                foreach (var property in changedPropertyNames) {
                    this.DbContext.Entry (entity).Property (property).IsModified = true;
                }
            }
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return AsQueryable().Any(predicate);
        }

        private static IQueryable<T> PerformInclusions (IEnumerable<Expression<Func<T, object>> > includeProperties,
            IQueryable<T> query) {
            return includeProperties.Aggregate (query, (current, includeProperty) => current.Include (includeProperty));
        }
    }
}