using System.Collections.Generic;
using IGMP.Model;

namespace IGMP.Service
{
    public interface IFilterService
    {
        IEnumerable<Importer> GetImporters(string filter);
        IEnumerable<Consignor> GetConsignors(string filter);
        IEnumerable<Item> GetItems(string filter);
        IEnumerable<BLNumber> GetBLNumbers(string filter);
        IEnumerable<FlightNumber> GetFlightNumbers(string filter);
        IEnumerable<Shed> GetSheds(string filter);
    }
}
