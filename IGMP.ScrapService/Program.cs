﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using IGMP.Data;
using Microsoft.EntityFrameworkCore;
using IGMP.Scraping.Scrapers;
using AutoMapper;
using IGMP.Data.Initializer;
using IGMP.Core;

namespace IGMP.Scraping
{
    class Program
    {
        private static IServiceCollection Services { get; set; }
        private static IServiceProvider Provider { get; set; }
        private static IConfigurationRoot Configuration { get; set; }
        private static ILogger<Program> Logger { get; set; }

        static void Main(string[] args)
        {
            // add the framework services
            Services = new ServiceCollection();
            
            AddAutoMapper();
            BuildConfiguration();

            RegisterLogger();
            RegisterDbContext();
            RegisterDI();

            AddFileLogger();


            Logger = Provider.GetService<ILoggerFactory>().CreateLogger<Program>();

            var dbContext = Provider.GetRequiredService<AppDbContext>();
            AppDbInitializer.Initialize(dbContext, Logger);

            Logger.LogInformation("Starting application");

            //do the actual work here
            var app = Provider.GetService<IExecutionService>();
            app.Run().Wait();

            Logger.LogInformation("Stopping application!");

            Console.ReadLine();
        }

        private static void AddFileLogger()
        {
            Provider
                .GetService<ILoggerFactory>()
                .AddFile(Configuration.GetSection("Logging"));
        }

        private static void AddAutoMapper()
        {
            Services.AddAutoMapper(mapperConfig => mapperConfig.AddProfiles(typeof(Program).Assembly));
            Provider = Services.BuildServiceProvider();
        }

        private static void RegisterDI()
        {
            // add StructureMap
            var container = new Container();
            container.Configure(config =>
            {
                // Register stuff in container, using the StructureMap APIs...
                config.Scan(_ =>
                {
                    _.AssemblyContainingType(typeof(Program));
                    _.WithDefaultConventions();
                });

                config.For<IExecutionService>().Use<ExecutionService>().Singleton();
                config.For<IExecutionContext>().Use<ExecutionContext>().Singleton();
                config.For<IRequestContext>().Use<RequestContext>().Singleton();
                config.For<ShedService>().Use<ShedService>().Singleton();
                config.For<IConfiguration>().Use(Configuration);

                // Populate the container using the service collection
                config.Populate(Services);
            });

            Provider = container.GetInstance<IServiceProvider>();
        }

        private static void RegisterDbContext()
        {
            Services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("AppDB"));
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            }, ServiceLifetime.Transient);
        }

        private static void RegisterLogger()
        {
            Services.AddLogging(config =>
            {
                config.AddConfiguration(Configuration.GetSection("Logging"));
                config.AddConsole();
            });
        }

        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
        }
    }
}
