﻿namespace IGMP.Data.Constants
{
    public static class Sequences
    {
        public const string FlightInfoId = "Sequence_FlightInfoId";
        public const string MasterIndexInfoId = "Sequence_MasterIndexInfoId";
        // public const string ConsoleIndexInfoId = "Sequence_ConsoleIndexInfoId";
        public const string SerialIndexInfoId = "Sequence_SerialIndexInfoId";
        public const string ShedRefInfoId = "Sequence_ShedRefInfoId"; 
    }
}
