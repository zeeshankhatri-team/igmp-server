﻿using System;
using System.Linq;
using System.Reflection;

namespace IGMP.Scraping.Extensions
{
    public static class AttributeExtensions
    {
        public static TValue GetAttributeValue<TAttribute, TValue>(
            this Type type,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            if (att != null)
            {
                return valueSelector(att);
            }

            return default(TValue);
        }

        public static TValue GetAttributeValue<TAttribute, TValue>(
            this PropertyInfo property,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            var att = property.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            if (att != null)
            {
                return valueSelector(att);
            }

            return default(TValue);
        }
    }
}
