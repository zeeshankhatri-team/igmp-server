using IGMP.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IGMP.Data
{
    public class FlightInfoConfiguration : IEntityTypeConfiguration<FlightInfo>
    {
        public void Configure(EntityTypeBuilder<FlightInfo> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedNever();
            builder.HasOne(i => i.Port).WithMany().HasForeignKey(i => i.PortId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}