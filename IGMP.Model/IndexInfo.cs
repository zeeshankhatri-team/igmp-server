using System;

namespace IGMP.Model {
    public class IndexInfo {
        public int PortId { get; set; }
        public int Year { get; set; }
        public int IndexNumber { get; set; }
        public int IndexSerialNumber { get; set; }
        public string ItemDescription { get; set; }
        public int IGMNumber { get; set; }
        public DateTime? IGMDate { get; set; }
        public int FlightInfoId { get; set; }
        public int IndexInfoId { get; set; }
        public string IndexType { get; set; }
        public string PortName { get; set; }
        public string PortCode { get; set; }
        public string PortSourceWebCode { get; set; }
        public string FlightNumber { get; set; }
        public string AirLine { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public string ImporterName { get; set; }
        public string ConsignorName { get; set; }
        public string BLNumber { get; set; }
        public DateTime? BLDate { get; set; }
        public string Shed { get; set; }
        public string ShedCode { get; set; }
        public string ShedName { get; set; }
        public int? ShedSourceWebId { get; set; }
        public string ShedSourceWebDesc { get; set; }
    }
}