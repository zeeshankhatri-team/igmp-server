﻿namespace IGMP.Model
{
    [Info(HtmlTableId = "dgShed", FirstColumnIndex = 0)]
    public class ShedRefInfo : BaseInfo
    {
        [InfoColumn(Index = -3, Caption = "Code")]
        public string Code { get; set; }

        [InfoColumn(Index = -4, Caption = "Name")]
        public string Name { get; set; }

        [InfoColumn(Index = 0, Caption = "Shed Id", DataType = typeof(int))]
        public int? SourceWebId { get; set; }

        [InfoColumn(Index = 1, Caption = "Shed Description")]
        public string SourceWebDesc { get; set; }
    }
}