using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace IGMP.Data {
    public class ViewRepository<T> : IViewRepository<T> where T : class {
        private DbQuery<T> DbQuery { get; set; }
        private DbContext DbContext { get; set; }

        public ViewRepository (AppDbContext context) {
            this.DbContext = context;
            this.DbQuery = context.Query<T> ();
        }

        public IQueryable<T> AsQueryable () {
            return this.DbQuery.AsNoTracking ();
        }

        public int Count()
        {
            return AsQueryable().Count();
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return AsQueryable().Count(predicate);
        }

        public IEnumerable<T> Find (Expression<Func<T, bool>> where) {
            IQueryable<T> query = AsQueryable ();
            return query.Where (where);
        }

        public T Single (Expression<Func<T, bool>> where) {
            IQueryable<T> query = AsQueryable ();
            return query.Single (where);
        }

        public T SingleOrDefault (Expression<Func<T, bool>> where) {
            IQueryable<T> query = AsQueryable ();
            return query.SingleOrDefault (where);
        }

        public T First (Expression<Func<T, bool>> where) {
            IQueryable<T> query = AsQueryable ();
            return query.First (where);
        }

        public T FirstOrDefault (Expression<Func<T, bool>> where) {
            IQueryable<T> query = AsQueryable ();
            return query.FirstOrDefault (where);
        }
    }
}