﻿using System;
using IGMP.Data;
using System.Linq;
using System.Collections.Generic;
using IGMP.Scraping.Constants;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using IGMP.Model;

namespace IGMP.Scraping.Scrapers
{
    public class IGMScraper : IIGMScraper
    {
        private IServiceProvider ServiceProvider { get; set; }
        private ILogger<IGMScraper> Logger { get; set; }
        private AppDbContext DbContext { get; set; }
        private IConfiguration Configuration { get; set; }

        private Port Port { get; set; }
        private string ViewState { get; set; }
        private string EventValidation { get; set; }
        private int Year { get; set; }

        private int EmptyLimit { get; set; }
        private int RetryLimit { get; set; }
        private int[] Ranges { get; set; }

        public IGMScraper(IServiceProvider serviceProvider,
            AppDbContext dbContext,
            IConfiguration configuration,
            ILogger<IGMScraper> logger)
        {
            this.ServiceProvider = serviceProvider;
            this.DbContext = dbContext;
            this.Configuration = configuration;
            this.Logger = logger;

            this.EmptyLimit = configuration.GetSection("ScrapService").GetValue<int>("EmptyLimit");
            this.RetryLimit = configuration.GetSection("ScrapService").GetValue<int>("RetryLimit");
            this.Ranges = configuration.GetSection("ScrapService").GetSection("Ranges").Get<int[]>();
        }

        public IDictionary<string, object> Scrap(IDictionary<string, object> dict)
        {
            this.Populate(dict);

            Logger.LogInformation($"Started scraping for Port: {this.Port.Code}, Year: {this.Year}".PadLeft(110));

            try
            {
                var metaInfos = this.DbContext.ScrapingMetaInfos.Where(info => info.PortId == this.Port.Id && info.Year == this.Year);

                int? previous = null;
                foreach(var range in this.Ranges)
                {
                    Logger.LogInformation($"From ({range}) for Port: {this.Port.Code}, Year: {this.Year}".PadLeft(110));
                    var max = metaInfos.Where(info => info.Number >= range && (previous == null || info.Number < previous.Value)).Select(info => info.Number).DefaultIfEmpty(range - 1).Max();
                    ScrapRange(metaInfos, max, range);
                    previous = range;
                }

                Logger.LogInformation($"Finished scraping for Port: {this.Port.Code}, Year: {this.Year}".PadLeft(110));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Failed for Port: {this.Port.Code}, Year: {this.Year}".PadLeft(110));
            }
             
            return null;
        }

        private void ScrapRange(IQueryable<ScrapingMetaInfo> metaInfos, int max, int start)
        {
            if (max > start)
            {
                var range = Enumerable.Range(start, max - start);
                var except = metaInfos.Where(info => info.Number >= start && info.Number <= max).Select(info => info.Number);
                var missings = range.Except(except);
                this.ScrapMissingIGM(missings);
            }

            this.ScrapIGMFrom(max + 1);
        }

        private void ScrapMissingIGM(IEnumerable<int> missings)
        {
            var type = "MISSING";

            foreach (var number in missings)
            {
                var retry = 0;
                
                do
                {
                    string retryText = Helper.GetRetryText(retry);

                    try
                    {
                        LogScraping(type, number, retryText);
                        var count = Scrap(number);
                        LogScrapedResult(type, number, retryText, count);
                        retry = 0;
                    }
                    catch (Exception ex)
                    {
                        LogScrapingError(type, number, retryText, ex);
                        if (++retry > this.RetryLimit) return;
                        // Task.Delay(2000).Wait();
                    }
                    // Task.Delay(1000).Wait();
                }
                while (retry > 0);
            }
        }

        private void ScrapIGMFrom(int number)
        {
            var type = "NEW";
            var empty = 0;
            var retry = 0;

            while (empty < this.EmptyLimit && retry < this.RetryLimit)
            {
                string retryText = Helper.GetRetryText(retry);

                try
                {
                    LogScraping(type, number, retryText);
                    var count = Scrap(number);
                    if (count > 0)
                    {
                        empty = 0;
                    }
                    else
                    {
                        empty++;
                    }

                    LogScrapedResult(type, number, retryText, count);

                    number++;
                    retry = 0;
                }
                catch (Exception ex)
                {
                    LogScrapingError(type, number, retryText, ex);
                    if (++retry > this.RetryLimit) return;
                    Task.Delay(2000).Wait();
                }
                Task.Delay(1000).Wait();
            }
        }

        private void Populate(IDictionary<string, object> dict)
        {
            this.Port = (Port)dict[ScrapingParams.Port];
            this.Year = 2018;
            this.ViewState = (string)dict[ScrapingParams.ViewState];
            this.EventValidation = (string)dict[ScrapingParams.EventValidation];
        }

        private int Scrap(int number)
        {
            var dict = new Dictionary<string, object>
            {
                { ScrapingParams.ViewState, this.ViewState },
                { ScrapingParams.EventValidation, this.EventValidation },
                { ScrapingParams.Port, this.Port },
                { ScrapingParams.Number, number },
                { ScrapingParams.Year, this.Year }
            };

            var request = ServiceProvider.GetService<IIGMRequest>();
            var handler = ServiceProvider.GetService<IIGMHandler>();
            var response = request.Send(dict);

            var result = handler.Handle(response, dict);
            return (int)result[ScrapingParams.Count];
        }

        private void LogScrapingError(string type, int number, string retryText, Exception ex)
        {
            var message = $"Scraping for '{type}' Port[{this.Port.Code}] Year[{Year}] Number[{number}]: FAILED{retryText}";
            Logger.LogInformation(message);
            Logger.LogDebug(ex, message);
        }

        private void LogScrapedResult(string type, int number, string retryText, int count)
        {
            var result = count > 0 ? $"Succeeded: {count}" : "No records found.";
            Logger.LogInformation($"Scraped for '{type}' Port[{this.Port.Code}] Year[{Year}] Number[{number}]: {result}{retryText}");
        }

        private void LogScraping(string type, int number, string retryText)
        {
            Logger.LogInformation($"Scraping for '{type}' Port[{this.Port.Code}] Year[{Year}] Number[{number}]{retryText}");
        }
    }
}
