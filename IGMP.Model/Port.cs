﻿namespace IGMP.Model
{
    public class Port
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public char Type { get; set; }

        public string SourceWebCode { get; set; }
        public string SourceWebName { get; set; }
    }

}
