﻿using System;

namespace IGMP.Model
{
    [Info(HtmlTableId = "MyDataGrid", FirstColumnIndex = 1)]
    public class FlightInfo : BaseInfo
    {
        [InfoColumn(Index = 0, Caption = "Flight Number")]
        public string FlightNumber { get; set; }

        [InfoColumn(Index = 1, Caption = "Airline")]
        public string AirLine { get; set; }

        [InfoColumn(Index = 2, Caption = "Departure Airport")]
        public string DepartureAirport { get; set; }

        [InfoColumn(Index = 3, Caption = "Arrival Date", DataType = typeof(DateTime))]
        public DateTime? ArrivalDate { get; set; }

        [InfoColumn(Index = 4, Caption = "IGM Date", DataType = typeof(DateTime))]
        public DateTime? IGMDate { get; set; }

    }
}