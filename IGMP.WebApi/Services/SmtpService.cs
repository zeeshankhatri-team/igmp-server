﻿using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace IGMP.WebApi.Services
{
    public class SmtpService : IEmailService
    {
        public SmtpService(IOptions<EmailSettings> emailSettings, IOptions<SmtpSettings> smtpSettings)
        {
            this.EmailSettings = emailSettings.Value;
            this.SmtpSettings = smtpSettings.Value;
        }

        private EmailSettings EmailSettings { get; set; }
        private SmtpSettings SmtpSettings { get; set; }

        public async Task SendAsync(Email email)
        {
            var message = new MailMessage(EmailSettings.FromEmail, email.To, email.Subject, email.Body);
            message.IsBodyHtml = true;
           
            using (var client = new SmtpClient())
            {
                client.Port = SmtpSettings.Port;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = SmtpSettings.Server;
                client.Credentials = new NetworkCredential(SmtpSettings.UserName, SmtpSettings.Password);
                await client.SendMailAsync(message);
            }
        }
    }
}
