﻿using Microsoft.Extensions.DependencyInjection;

namespace IGMP.Core
{
    public static class Registry
    {
        public static void RegisterCore(this IServiceCollection services)
        {
            services.AddScoped<IRequestContext, RequestContext>();
        }
    }
}
