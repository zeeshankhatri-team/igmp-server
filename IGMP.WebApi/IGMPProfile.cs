using AutoMapper;
using IGMP.Model;
using IGMP.WebApi.Auth.Model;
using IGMP.WebApi.Model;

namespace IGMP.WebApi
{
    public class IGMPProfile : Profile {
        public IGMPProfile () {
            CreateMap<AppUser, RegisterModel>();
            CreateMap<RegisterModel, AppUser>();
        }
    }
}