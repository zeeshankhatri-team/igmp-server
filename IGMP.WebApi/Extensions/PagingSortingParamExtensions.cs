﻿using IGMP.Service.Model;

namespace IGMP.WebApi.Extensions
{
    public static class PagingSortingParamExtensions
    {
        public static PagedSorted<T> ToPagedSorted<T>(this PagingSortingParam queryParams) 
            where T : new()
        {
            var pagedSorted = new PagedSorted<T>()
            {
                PageIndex = queryParams.PageIndex,
                PageSize = queryParams.PageSize,
                SortBy = queryParams.SortBy,
                Direction = queryParams.Direction,
            };

            return pagedSorted;
        }

    }
}